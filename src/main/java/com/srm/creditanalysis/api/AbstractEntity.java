package com.srm.creditanalysis.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.srm.creditanalysis.api.domain.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@JsonIgnoreProperties({
        "createdBy",
        "updatedBy",
        "created",
        "updated",
        "active"
})
@MappedSuperclass
public abstract class AbstractEntity implements Traceable {

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private User createdBy;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "updated_by")
    private User updatedBy;

    @Getter
    @Setter
    @Column(name = "created")
    private LocalDate created;

    @Getter
    @Setter
    @Column(name = "updated")
    private LocalDate updated;

    @Getter
    @Setter
    @Column(name = "active")
    private Boolean active;
}
