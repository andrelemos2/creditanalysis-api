package com.srm.creditanalysis.api.resource;

import com.srm.creditanalysis.api.domain.BusinessPartner;
import com.srm.creditanalysis.api.event.CreatedResourceEvent;
import com.srm.creditanalysis.api.repository.BusinessPartnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("credit-analysis-api")
public class BusinessPartnerResource {

    @Autowired
    private BusinessPartnerRepository repository;

    @Autowired
    private ApplicationEventPublisher publisher;

    @GetMapping("partners-list")
    public ResponseEntity<List<BusinessPartner>> list() {
        List<BusinessPartner> partners = repository.findAll();
        return partners.size() > 0 ? ResponseEntity.ok(partners) : ResponseEntity.notFound().build();
    }

    @PostMapping("partners-creste")
    public ResponseEntity<BusinessPartner> create(@Valid @RequestBody BusinessPartner partner, HttpServletResponse response) {
        BusinessPartner partnerSaved = repository.save(partner);
        publisher.publishEvent(new CreatedResourceEvent(this, response, partnerSaved.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(partnerSaved);
    }

    @GetMapping("partners/id")
    public ResponseEntity<BusinessPartner> findById(@PathVariable Long id) {
        Optional<BusinessPartner> partnerOptional = repository.findById(id);
        return partnerOptional.isPresent() ? ResponseEntity.ok(partnerOptional.get()) : ResponseEntity.notFound().build();
    }

    @GetMapping("partners/findbyname/{name}")
    @ResponseBody
    public List<BusinessPartner> getByPartialName(@PathVariable final String name) {
        return repository.findByPartialName(name);
    }
}
