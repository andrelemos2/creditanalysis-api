package com.srm.creditanalysis.api.resource;

import com.srm.creditanalysis.api.domain.OperationTax;
import com.srm.creditanalysis.api.repository.OperationTaxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("credit-analysis-api")
public class OperationTypeResource {

    @Autowired
    private OperationTaxRepository repository;

    @GetMapping("taxes-list")
    public ResponseEntity<List<OperationTax>> list() {
        List<OperationTax> operationTaxes = repository.findAll();
        return operationTaxes.size() > 0 ? ResponseEntity.ok(operationTaxes) : ResponseEntity.notFound().build();
    }
}
