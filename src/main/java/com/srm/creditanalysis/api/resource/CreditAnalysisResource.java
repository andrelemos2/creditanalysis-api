package com.srm.creditanalysis.api.resource;

import com.srm.creditanalysis.api.domain.CreditAnalysis;
import com.srm.creditanalysis.api.event.CreatedResourceEvent;
import com.srm.creditanalysis.api.repository.CreditAnalysisRepository;
import com.srm.creditanalysis.api.service.CreditAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("credit-analysis-api")
public class CreditAnalysisResource {

    @Autowired
    private CreditAnalysisRepository repository;

    @Autowired
    private CreditAnalysisService service;

    @Autowired
    private ApplicationEventPublisher publisher;

    @GetMapping("analysis-list")
    public ResponseEntity<List<CreditAnalysis>> list() {
        List<CreditAnalysis> creditAnalyses = repository.findAll();
        return creditAnalyses.size() > 0 ? ResponseEntity.ok(creditAnalyses) : ResponseEntity.notFound().build();
    }

    @PostMapping("analysis-create")
    public ResponseEntity<CreditAnalysis> create(@Valid @RequestBody CreditAnalysis analysis, HttpServletResponse response) {
        CreditAnalysis creditAnalysisSaved = service.save(analysis);
        publisher.publishEvent(new CreatedResourceEvent(this, response, creditAnalysisSaved.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(creditAnalysisSaved);
    }

    @PutMapping("analysis/{id}")
    public ResponseEntity<CreditAnalysis> update(@PathVariable Long id, @Valid @RequestBody CreditAnalysis creditAnalysis) {
        CreditAnalysis creditAnalysisSaved = service.update(id, creditAnalysis);
        return ResponseEntity.ok(creditAnalysisSaved);
    }

    @DeleteMapping("analysis/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
