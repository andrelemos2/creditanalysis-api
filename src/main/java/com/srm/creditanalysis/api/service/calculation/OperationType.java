package com.srm.creditanalysis.api.service.calculation;


import com.srm.creditanalysis.api.service.tax.RiskA;
import com.srm.creditanalysis.api.service.tax.RiskB;
import com.srm.creditanalysis.api.service.tax.RiskC;

public enum  OperationType {

    A("Risk A"){
        @Override
        public CalculateRisk getTax() {
            return new RiskA();
        }
    },
    B("Risk B"){
        @Override
        public CalculateRisk getTax() {
            return new RiskB();
        }
    },
    C("Risk C"){
        @Override
        public CalculateRisk getTax() {
            return new RiskC();
        }
    };

    private final String name;

    OperationType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract CalculateRisk getTax();

}
