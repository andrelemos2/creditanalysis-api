package com.srm.creditanalysis.api.service.calculation;

import com.srm.creditanalysis.api.domain.CreditAnalysis;

import java.math.BigDecimal;

public interface CalculateRisk {
    BigDecimal analyze(CreditAnalysis creditAnalysis);
}
