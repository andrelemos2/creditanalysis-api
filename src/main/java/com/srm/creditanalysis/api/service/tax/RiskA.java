package com.srm.creditanalysis.api.service.tax;

import com.srm.creditanalysis.api.domain.CreditAnalysis;
import com.srm.creditanalysis.api.service.calculation.CalculateRisk;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RiskA implements CalculateRisk {

    private final BigDecimal INTEREST_AMMOUNT = BigDecimal.ZERO;

    @Override
    public BigDecimal analyze(CreditAnalysis creditAnalysis) {
        return creditAnalysis.getCreditLimmit().multiply(INTEREST_AMMOUNT).setScale(2, RoundingMode.HALF_UP);
    }
}
