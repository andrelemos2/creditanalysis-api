package com.srm.creditanalysis.api.service;

import com.srm.creditanalysis.api.domain.BusinessPartner;
import com.srm.creditanalysis.api.domain.CreditAnalysis;
import com.srm.creditanalysis.api.domain.OperationTax;
import com.srm.creditanalysis.api.repository.BusinessPartnerRepository;
import com.srm.creditanalysis.api.repository.CreditAnalysisRepository;
import com.srm.creditanalysis.api.repository.OperationTaxRepository;
import com.srm.creditanalysis.api.service.calculation.CalculateRisk;
import com.srm.creditanalysis.api.service.calculation.OperationType;
import com.srm.creditanalysis.api.service.exception.BusinessPartnerNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class CreditAnalysisService {

    @Autowired
    private CreditAnalysisRepository repository;

    @Autowired
    private BusinessPartnerRepository partnerRepository;

    @Autowired
    private OperationTaxRepository operationTypeRepository;

    public CreditAnalysis save(CreditAnalysis creditAnalysis) {

        partnerValid(creditAnalysis);

        operationTaxValid(creditAnalysis);

        LocalDate created = LocalDate.now();

        calculateOperation(creditAnalysis);

        creditAnalysis.setActive(true);
        creditAnalysis.setCreated(created);

        return repository.save(creditAnalysis);
    }

    private void calculateOperation(CreditAnalysis creditAnalysis) {
        OperationType operationType = creditAnalysis.getOperationTax().getCode();
        CalculateRisk tax = operationType.getTax();
        BigDecimal interestAmmount = tax.analyze(creditAnalysis);
        BigDecimal creditLimmitAmmountWithTax = creditAnalysis.getCreditLimmit().add(interestAmmount);

        creditAnalysis.setInterestAmmount(interestAmmount);
        creditAnalysis.setCreditLimmitWithTax(creditLimmitAmmountWithTax);
    }

    private void operationTaxValid(CreditAnalysis creditAnalysis) {
        if (creditAnalysis.getOperationTax().getId() != null) {
            return;
        }

        OperationTax operationTax = operationTypeRepository.findByName(creditAnalysis.getOperationTax().getName());
        BeanUtils.copyProperties(operationTax, creditAnalysis.getOperationTax());
    }

    private void partnerValid(CreditAnalysis creditAnalysis) {
        Optional<BusinessPartner> partnerOptional = Optional.empty();
        if (creditAnalysis.getBusinessPartner().getId() != null) {
            partnerOptional = Optional.ofNullable(partnerRepository.findByName(creditAnalysis.getBusinessPartner().getName()));
        }

        Optional<BusinessPartner> partnerSavedOptional = Optional.empty();
        if (!partnerOptional.isPresent()) {
            partnerSavedOptional = Optional.ofNullable(partnerRepository.save(creditAnalysis.getBusinessPartner()));
        }

        if (!partnerSavedOptional.isPresent() && !partnerOptional.isPresent()) {
            throw new BusinessPartnerNotFoundException();
        }
    }

    public CreditAnalysis update(Long id, CreditAnalysis creditAnalysis) {
        CreditAnalysis creditAnalysisSaved = findCreditAnalysis(id);
        calculateOperation(creditAnalysis);
        BeanUtils.copyProperties(creditAnalysis, creditAnalysisSaved, "id");
        return repository.save(creditAnalysisSaved);
    }

    private CreditAnalysis findCreditAnalysis(Long id) {
        Optional<CreditAnalysis> creditAnalysisSaved = repository.findById(id);
        if (!creditAnalysisSaved.isPresent()) {
            throw new IllegalArgumentException();
        }
        return creditAnalysisSaved.get();
    }
}
