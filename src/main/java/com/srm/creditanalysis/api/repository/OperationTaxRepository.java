package com.srm.creditanalysis.api.repository;

import com.srm.creditanalysis.api.domain.OperationTax;
import com.srm.creditanalysis.api.repository.operationtax.OperationTaxRepositoryQuery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationTaxRepository extends JpaRepository<OperationTax, Long>, OperationTaxRepositoryQuery {
}
