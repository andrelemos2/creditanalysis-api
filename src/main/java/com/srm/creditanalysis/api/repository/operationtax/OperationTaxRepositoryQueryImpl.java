package com.srm.creditanalysis.api.repository.operationtax;

import com.srm.creditanalysis.api.domain.OperationTax;
import com.srm.creditanalysis.api.service.calculation.OperationType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public class OperationTaxRepositoryQueryImpl implements OperationTaxRepositoryQuery {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public OperationTax findByName(String name) {

        final StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("Select o From OperationTax As o ");
        stringQuery.append("WHERE o.name = :name");

        TypedQuery<OperationTax> query = manager.createQuery(stringQuery.toString(), OperationTax.class);
        query.setParameter("name", name);

        return query.getSingleResult();
    }
}
