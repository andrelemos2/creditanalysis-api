package com.srm.creditanalysis.api.repository;

import com.srm.creditanalysis.api.domain.CreditAnalysis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditAnalysisRepository extends JpaRepository<CreditAnalysis, Long> {
}
