package com.srm.creditanalysis.api.repository.partner;

import com.srm.creditanalysis.api.domain.BusinessPartner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class BusinessPartnerRepositoryQueryImpl implements BusinessPartnerRepositoryQuery {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public List<BusinessPartner> findByPartialName(String name) {

        final StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("Select p From BusinessPartner As p ");
        stringQuery.append("WHERE lower(p.name) like lower(concat('%', :name,'%'))");

        TypedQuery<BusinessPartner> query = manager.createQuery(stringQuery.toString(), BusinessPartner.class);
        query.setParameter("name", name);

        return query.getResultList();
    }
}
