package com.srm.creditanalysis.api.repository;

import com.srm.creditanalysis.api.domain.BusinessPartner;
import com.srm.creditanalysis.api.repository.partner.BusinessPartnerRepositoryQuery;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BusinessPartnerRepository extends JpaRepository<BusinessPartner, Long>, BusinessPartnerRepositoryQuery {
    BusinessPartner findByName(String name);
}
