package com.srm.creditanalysis.api.repository.operationtax;

import com.srm.creditanalysis.api.domain.OperationTax;

public interface OperationTaxRepositoryQuery {
    OperationTax findByName(String name);
}
