package com.srm.creditanalysis.api.repository.partner;

import com.srm.creditanalysis.api.domain.BusinessPartner;

import java.util.List;

public interface BusinessPartnerRepositoryQuery {
    List<BusinessPartner> findByPartialName(String name);
}
