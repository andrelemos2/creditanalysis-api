package com.srm.creditanalysis.api.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@EqualsAndHashCode
@Entity
@Table(name = "ad_permission")
public class Permission {

    @Getter
    @Setter
    @Id
    private Long id;

    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private String description;
}
