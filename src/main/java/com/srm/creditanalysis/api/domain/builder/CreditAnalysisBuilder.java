package com.srm.creditanalysis.api.domain.builder;

import com.srm.creditanalysis.api.domain.BusinessPartner;
import com.srm.creditanalysis.api.domain.CreditAnalysis;
import com.srm.creditanalysis.api.domain.OperationTax;
import com.srm.creditanalysis.api.service.calculation.CalculateRisk;
import com.srm.creditanalysis.api.service.calculation.OperationType;

import java.math.BigDecimal;

public class CreditAnalysisBuilder {

    private CreditAnalysis instance;

    public CreditAnalysisBuilder() {
        this.instance = new CreditAnalysis();
    }

    public CreditAnalysisBuilder withBusinessPartner(final BusinessPartner partner) {
        this.instance.setBusinessPartner(partner);
        return this;
    }

    public CreditAnalysisBuilder withOperationTax(final OperationTax operationTax) {
        this.instance.setOperationTax(operationTax);
        return this;
    }

    public CreditAnalysisBuilder withCreditLimmit(final BigDecimal creditLimmitAmmount) {
        this.instance.setCreditLimmit(creditLimmitAmmount);
        return this;
    }

    public CreditAnalysisBuilderValid withCalculateRisk() {
        OperationType operationType = this.instance.getOperationTax().getCode();
        CalculateRisk tax = operationType.getTax();
        BigDecimal interestAmmount = tax.analyze(this.instance);
        BigDecimal creditLimmitAmmountWithTax = this.instance.getCreditLimmit().add(interestAmmount);
        this.instance.setCreditLimmitWithTax(creditLimmitAmmountWithTax);
        return new CreditAnalysisBuilderValid(instance);
    }
}
