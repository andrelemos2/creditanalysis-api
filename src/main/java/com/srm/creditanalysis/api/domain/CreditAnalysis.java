package com.srm.creditanalysis.api.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.srm.creditanalysis.api.AbstractEntity;
import com.srm.creditanalysis.api.service.calculation.OperationType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "businessPartner",
        "operationTax",
        "creditLimmit",
        "interestAmmount",
        "creditLimmitWithTax"
})
@Entity
@Table(name = "credit_analyze")
public class CreditAnalysis extends AbstractEntity {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @NotNull
    @ManyToOne
    @JoinColumn(name = "bpartner_id")
    private BusinessPartner businessPartner;

    @Getter
    @Setter
    @NotNull
    @ManyToOne
    @JoinColumn(name = "operation_tax_id")
    private OperationTax operationTax;

    @Getter
    @Setter
    @Column(name = "interest_ammount")
    private BigDecimal interestAmmount = BigDecimal.ZERO;

    @Getter
    @Setter
    @NotNull
    @Column(name = "credit_limmit")
    private BigDecimal creditLimmit = BigDecimal.ZERO;

    @Getter
    @Setter
    @Column(name = "credit_limmit_tax")
    private BigDecimal creditLimmitWithTax = BigDecimal.ZERO;

}
