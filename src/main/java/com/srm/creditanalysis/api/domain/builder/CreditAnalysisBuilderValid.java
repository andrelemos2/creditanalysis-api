package com.srm.creditanalysis.api.domain.builder;

import com.srm.creditanalysis.api.domain.CreditAnalysis;

public class CreditAnalysisBuilderValid {

    private CreditAnalysis instance;

    public CreditAnalysisBuilderValid(CreditAnalysis instance) {
        this.instance = instance;
    }

    public CreditAnalysis build() {
        return this.instance;
    }
}
