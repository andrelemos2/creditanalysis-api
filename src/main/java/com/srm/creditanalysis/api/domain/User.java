package com.srm.creditanalysis.api.domain;

import com.srm.creditanalysis.api.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@EqualsAndHashCode
@Entity
@Table(name = "ad_user")
public class User extends AbstractEntity {

    @Getter
    @Setter
    @Id
    private Long id;

    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private String name;

    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    @Email
    private String email;

    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private String password;

    @Getter
    @Setter
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ad_user_permission", joinColumns = @JoinColumn(name = "ad_user_id")
            , inverseJoinColumns = @JoinColumn(name = "ad_permission_id"))
    private List<Permission> permissions;
}
