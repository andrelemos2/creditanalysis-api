package com.srm.creditanalysis.api;

import com.srm.creditanalysis.api.domain.User;

import java.io.Serializable;
import java.time.LocalDate;

public interface Traceable extends Serializable {

    User getUpdatedBy();

    User getCreatedBy();

    LocalDate getUpdated();

    LocalDate getCreated();

    Boolean getActive();
}
