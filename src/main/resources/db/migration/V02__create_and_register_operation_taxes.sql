DROP TABLE IF EXISTS operation_tax;

CREATE TABLE operation_tax (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	code VARCHAR(20) NOT NULL,
	name VARCHAR(20) NOT NULL,
  tax DECIMAL(10,2) NOT NULL DEFAULT 0,
  created_by BIGINT(20),
  updated_by BIGINT(20),
  created DATE DEFAULT (current_date),
  updated DATE DEFAULT (current_date),
  active BOOLEAN DEFAULT 1,
  FOREIGN KEY (created_by) REFERENCES ad_user(id),
  FOREIGN KEY (updated_by) REFERENCES ad_user(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into operation_tax (code, name, tax) values ('A', 'A', 0.0);
insert into operation_tax (code, name, tax) values ('B', 'B', 0.1);
insert into operation_tax (code, name, tax) values ('C', 'C', 0.2);