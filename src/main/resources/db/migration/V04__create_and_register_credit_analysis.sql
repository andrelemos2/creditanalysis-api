DROP TABLE IF EXISTS credit_analyze;

CREATE TABLE credit_analyze (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	bpartner_id BIGINT(20) NOT NULL,
	operation_tax_id BIGINT(20) NOT NULL,
  credit_limmit DECIMAL(10,2) NOT NULL DEFAULT 0,
  interest_ammount DECIMAL(10,2) DEFAULT 0,
  credit_limmit_tax DECIMAL(10,2) DEFAULT 0,
  created_by BIGINT(20),
  updated_by BIGINT(20),
  created DATE DEFAULT (current_date),
  updated DATE DEFAULT (current_date),
  active BOOLEAN DEFAULT 1,
  FOREIGN KEY (operation_tax_id) REFERENCES operation_tax(id),
  FOREIGN KEY (bpartner_id) REFERENCES bpartner(id),
  FOREIGN KEY (created_by) REFERENCES ad_user(id),
  FOREIGN KEY (updated_by) REFERENCES ad_user(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO credit_analyze (bpartner_id, operation_tax_id, credit_limmit, credit_limmit_tax) VALUES (1, 1, 1050.00, 1050.00);
INSERT INTO credit_analyze (bpartner_id, operation_tax_id, credit_limmit, credit_limmit_tax) VALUES (1, 2, 2000.00, 2000.00);
INSERT INTO credit_analyze (bpartner_id, operation_tax_id, credit_limmit, credit_limmit_tax) VALUES (1, 3, 3050.00, 3050.00);