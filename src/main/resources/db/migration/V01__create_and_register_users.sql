DROP TABLE IF EXISTS ad_user;

CREATE TABLE ad_user (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	email VARCHAR(255),
	password VARCHAR(150) NOT NULL,
  created_by BIGINT(20),
  updated_by BIGINT(20),
  created DATE DEFAULT (current_date),
  updated DATE DEFAULT (current_date),
  active BOOLEAN DEFAULT 1,
  FOREIGN KEY (created_by) REFERENCES ad_user(id),
  FOREIGN KEY (updated_by) REFERENCES ad_user(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
