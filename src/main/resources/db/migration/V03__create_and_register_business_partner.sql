DROP TABLE IF EXISTS bpartner;

CREATE TABLE bpartner (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(255),
	vendor BOOLEAN DEFAULT 0,
  created_by BIGINT(20),
  updated_by BIGINT(20),
  created DATE DEFAULT (current_date),
  updated DATE DEFAULT (current_date),
  active BOOLEAN DEFAULT 1,
  FOREIGN KEY (created_by) REFERENCES ad_user(id),
  FOREIGN KEY (updated_by) REFERENCES ad_user(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO bpartner (name, description) VALUES ('Itaú', 'Banco privado');