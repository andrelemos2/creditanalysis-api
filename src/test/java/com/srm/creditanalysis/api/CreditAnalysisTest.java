package com.srm.creditanalysis.api;

import com.srm.creditanalysis.api.domain.BusinessPartner;
import com.srm.creditanalysis.api.domain.CreditAnalysis;
import com.srm.creditanalysis.api.domain.OperationTax;
import com.srm.creditanalysis.api.domain.builder.CreditAnalysisBuilder;
import com.srm.creditanalysis.api.service.calculation.OperationType;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;

public class CreditAnalysisTest {

    private BusinessPartner partner;

    @Before
    public void init() {
        partner = new BusinessPartner();
        partner.setName("Itaú");
        partner.setDescription("Banco privado");
        partner.setVendor(false);
    }

    @Test
    public void calculateRiskTaxTypeA() {
        BigDecimal creditLimmitAmmount = BigDecimal.TEN;

        OperationTax operationTax = new OperationTax();
        operationTax.setCode(OperationType.A);
        operationTax.setTax(new BigDecimal(0.0));

        CreditAnalysis creditAnalysis = new CreditAnalysisBuilder()
                .withBusinessPartner(partner)
                .withCreditLimmit(creditLimmitAmmount)
                .withOperationTax(operationTax)
                .withCalculateRisk()
                .build();

        final BigDecimal creditLimmitWithTax = creditAnalysis.getCreditLimmitWithTax();

        final BigDecimal expectedAmmount = BigDecimal.TEN.setScale(2, BigDecimal.ROUND_HALF_UP);

        assertEquals(expectedAmmount, creditLimmitWithTax);
    }

    @Test
    public void calculateRiskTaxTypeB() {
        BigDecimal creditLimmitAmmount = new BigDecimal(100.00);

        OperationTax operationTax = new OperationTax();
        operationTax.setCode(OperationType.B);
        operationTax.setTax(new BigDecimal(0.1));

        CreditAnalysis creditAnalysis = new CreditAnalysisBuilder()
                .withBusinessPartner(partner)
                .withCreditLimmit(creditLimmitAmmount)
                .withOperationTax(operationTax)
                .withCalculateRisk()
                .build();

        final BigDecimal creditLimmitAmmountWithTax = creditAnalysis.getCreditLimmitWithTax();

        final BigDecimal expectedAmmount = new BigDecimal(110).setScale(2, RoundingMode.HALF_UP);

        assertEquals(expectedAmmount, creditLimmitAmmountWithTax);
    }
}
