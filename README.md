Crédito Fácil (Análise de crédito)
==========================
Simples Projeto para análise de crédito

Última Versão
==========================
https://bitbucket.org/andrelemos2/creditanalysis-api/overview

Backlog
==========================
https://trello.com/b/wA5CTWvm/cr%C3%A9dito-f%C3%A1cil

Construindo na sua máquina
==========================
- Instância banco de dados MYSQL rodando na porta padrão 3306, user (root) e password(123) - (última versão com teste 8.0.13)
- git clone https://bitbucket.org/andrelemos2/creditanalysis-api.git
- mvn spring-boot:run

Opção de instância para o banco de dados
==========================
- docker run --name instance-mysql -e MYS   QL_ROOT_PASSWORD=123 -e MYSQL_DATABASE=creditanalysisapi -d mysql:latest

Técnologias utilizadas
==========================
- Spring Boot (https://spring.io/projects/spring-boot)
- FlywayDB (https://flywaydb.org)
- MYSQL (https://www.mysql.com)
- Maven (https://maven.apache.org)
- JUnit (https://junit.org)
- Java 8 (https://www.java.com/pt_BR/download/faq/java8.xml)
